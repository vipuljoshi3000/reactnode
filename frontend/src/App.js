import React, { Component } from 'react';
import Home from './components/Home';
import About from './components/About';
import ContactUs from './components/ContactUs';

import { BrowserRouter as Router, Route, Link} from "react-router-dom";

class App extends Component {
  
render() {
    return (
      <body>
       <Router>
       <div class='header' id='myHeader'>
          <Link class='link' to="/contact">ContactUs</Link>
           <Link class='link' to="/about">About</Link>
            <Link class='link' to="/">Home</Link>  
        </div>
        <div>
            <Route path="/" exact component={Home} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={ContactUs} />
            </div>
       </Router>
      </body>
      
      
    );
  }
}

export default App;